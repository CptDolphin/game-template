package com.sda.dp.game.model;

import java.awt.*;

public class GameText extends AbstractGameObject {

    private String text;

    public GameText(int x, int y, String text) {
        super(new Point(x, y));
        this.text = text;
    }

    @Override
    public void paint(Graphics2D g2d) {
        Color currentColor = g2d.getColor();

        g2d.setColor(Color.RED);
        g2d.drawString(text, this.position.x, this.position.y );

        g2d.setColor(currentColor);
    }
}
