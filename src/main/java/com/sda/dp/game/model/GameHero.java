package com.sda.dp.game.model;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameHero extends AbstractGameObject {

    private BufferedImage image;

    public GameHero(int x, int y) {
        super(new Point(x, y));
        try {
            image = ImageIO.read(new File("src/main/resources/hero.png"));
        } catch (IOException ex) {
            // handle exception...
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        g2d.drawImage(image, position.x, position.y, null);
    }
}
